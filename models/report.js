let mongoose = require("mongoose");
let bcrypt = require("bcrypt");

let reportSchema = new mongoose.Schema({
    contractId: mongoose.Types.ObjectId,
    reportDate: Date,
    reportMessage: Date,
    reportReply: String,
    status: String,
    dateEnd: Date
});


module.exports = mongoose.model("Report", reportSchema);