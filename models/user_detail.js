let mongoose = require("mongoose");
let bcrypt = require("bcrypt");
const ObjectId = mongoose.Types.ObjectId;

let user_detailSchema = new mongoose.Schema({
    idUser: {type: ObjectId, unique: true},
    image: String,
    description: String,
    address: String,
    creditCard: String,
    price: Number,
    tag:[String]
});

module.exports = mongoose.model("user_detail", user_detailSchema);