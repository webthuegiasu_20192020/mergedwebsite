let mongoose = require("mongoose");
let bcrypt = require("bcrypt");

let feedbackSchema = new mongoose.Schema({
    idContract: {type: mongoose.Types.ObjectId, require: true},
    state: Boolean,
    content: String,
    result: String
});


module.exports = mongoose.model("Feedback", feedbackSchema);