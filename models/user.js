let mongoose = require("mongoose");
let bcrypt = require("bcrypt");

let userSchema = new mongoose.Schema({
    username: String,
    password: String,
    firstname: String,
    lastname: String,
    email: {type:String,unique:true},
    address: String,
    type: Number,
    status: String
});

module.exports = mongoose.model("User", userSchema);