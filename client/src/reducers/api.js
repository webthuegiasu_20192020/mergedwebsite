import { REDIRECT_API, STOP_REDIRECT_API } from "../actions/actions";

const apiAction = (state = { isRedirect: false, URL: "" }, action) => {
  switch (action.type) {
    case REDIRECT_API:
      return {
        isRedirect: true,
        URL: action.URL
      };
    case STOP_REDIRECT_API:
      return {
        isRedirect: false,
        URL: ""
      };
    default:
      return state;
  }
};

export default apiAction;
