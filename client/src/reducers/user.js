import { GET_USER, GET_TOKEN, GET_ALL_USER, LOG_OUT } from "../actions/actions";

const user = (state = { data: {id:null}, token: null }, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        data: action.data,
        token: state.token
      };
    case GET_TOKEN:
      return {
        ...state,
        token: action.token
      };
    case GET_ALL_USER:
      return action.user;
    case LOG_OUT:
      return {
        data: null,
        token: null
      };
    default:
      return state;
  }
};

export default user;
