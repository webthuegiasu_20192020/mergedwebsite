import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import AdminNavBar from "../AdminNavBar";
import ReactPaginate from "react-paginate";
import { Table, Spinner, Button } from "react-bootstrap";
import "./UserListPage.css";

const UserListPage = ({ token, offSet }) => {
  let [dataAdmin, setData] = useState([{ username: "", id: "", email: "" }]);
  let [isLoading, setLoading] = useState(true);
  let [refresh, setRefresh] = useState(false);
  let [currentPage, setPage] = useState(0);
  let [pageCount, setPageCount] = useState(1);
  useEffect(() => {
    const fetchData = async (currentPage, offSet) => {
      const indexStart = currentPage * offSet;
      fetch(`/admin/user?indexStart=${indexStart}&offSet=${offSet}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(data => {
          setLoading(false);
          setData(data.array);
          setPageCount(data.pageCount / offSet);
        });
    };
    fetchData(currentPage, offSet);
  }, [token, offSet, currentPage, refresh]);
  const dataTable = dataAdmin.map((value, index) => {
    return (
      <tr key={index}>
        <td>{index}</td>
        <td>{value.username}</td>
        <td>{value.email}</td>
        <td>
          <Link to={`/admin/user/${value._id}`} className="btn btn-primary">
            Detail
          </Link>
        </td>
      </tr>
    );
  });

  const handlePageClick = data => {
    setLoading(true);
    setPage(data.selected);
  };

  const handleRefresh = () => {
    setRefresh(!refresh);
    setLoading(true);
  };
  return (
    <div>
      <AdminNavBar />
      <div>
        <div className="Content">
          <Button variant="info" type="button" onClick={handleRefresh}>
            Refresh
          </Button>
          <Table striped hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Username</th>
                <th>Email</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {isLoading ? (
                <tr>
                  <td colSpan="4" className="text-center">
                    <Spinner animation="border" variant="light" />
                  </td>
                </tr>
              ) : (
                dataTable
              )}
            </tbody>
          </Table>
          <div className="d-flex justify-content-center">
            <ReactPaginate
              previousLabel={"Previous"}
              nextLabel={"Next"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={data => handlePageClick(data)}
              containerClassName={"pagination"}
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              activeClassName={"active"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserListPage;
