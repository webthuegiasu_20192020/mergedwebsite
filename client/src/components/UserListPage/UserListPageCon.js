import { connect } from "react-redux";
import UserListPage from "./UserListPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    offSet: ownProps.offSet
  };
};

export default connect(mapStateToProps)(UserListPage);
