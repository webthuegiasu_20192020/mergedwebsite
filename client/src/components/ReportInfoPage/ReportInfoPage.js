import React, { useEffect, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import AdminNavBar from "../AdminNavBar";
import ContractDetail from "../ContractDetail";
import { Button, Form } from "react-bootstrap";
import { Form as ReduxForm, Control } from "react-redux-form";
import "./ReportInfoPage.css";

const bootstrapFormControl = props => {
  return <Form.Control {...props} />;
};

const ReportInfoPage = ({ match, token, onLoad, handleSubmit, banTutor }) => {
  useEffect(() => {
    onLoad(match.params.id);
  }, [match, onLoad]);

  const [dataReport, setDataReport] = useState({
    id: "",
    contractId: "",
    reportDate: "",
    reportMessage: "",
    reportReply: "",
    status: "",
    dateEnd: ""
  });

  useEffect(() => {
    const fetchData = async (match, token) => {
      fetch(`/admin/contract/${match.params.id}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(setDataReport);
    };
    fetchData(match, token);
  }, [match, token]);

  const [dataContract, setDataContract] = useState({
    id: "",
    tutor_id: "",
    tutor_name: "",
    student_id: "",
    student_name: "",
    condition: "",
    skill: "",
    hours: "",
    status: "",
    date_send: "",
    date_accept: "",
    date_finish: ""
  });
  useEffect(() => {
    const fetchData = async (id, token) => {
      fetch(`/admin/contract/${id}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(setDataContract);
    };
    fetchData(dataReport.contractId, token);
  }, [dataReport.contractId, token]);
  const [isRedirect, MakeRedirect] = useState(false);

  const handleClick = v => {
    handleSubmit(v);
    MakeRedirect(true);
  };

  if (isRedirect) {
    return <Redirect to="/admin/report" />;
  }

  return (
    <div>
      <AdminNavBar />
      <div className="Content">
        <Link to="/admin/report" className="btn btn-success">
          Return to Reports List
        </Link>
        <ContractDetail data={dataContract} />
        <div className="mt-2">
          <h5>Report Info:</h5>
          <div className="user-info mt-2">
            <div className="d-flex flex-row">
              <p className="font-weight-bold mr-2">Report message:</p>
              {dataReport.reportMessage}
            </div>
            <div className="d-flex flex-row">
              <p className="font-weight-bold mr-2">Report Date:</p>
              {dataReport.reportDate}
            </div>
            <div className="d-flex flex-row">
              <p className="font-weight-bold mr-2">Date end:</p>
              {dataReport.dateEnd}
            </div>
            <div className="d-flex flex-row">
              <p className="font-weight-bold mr-2">Status:</p>
              {dataReport.status}
            </div>
          </div>
        </div>
        <ReduxForm model="initialStateReport" onSubmit={v => handleClick(v)}>
          <Form.Group controlId="formBasicUsername">
            <Form.Label>Reply:</Form.Label>
            <Control.text
              model=".reportReply"
              component={bootstrapFormControl}
              type="text"
            />
          </Form.Group>
          <Button variant="primary" type="submit" className="ml-1">
            Done
          </Button>
          <Button
            variant="danger"
            type="button"
            className="ml-1"
            onClick={()=>banTutor(dataContract.tutor_id)}
          >
            Ban
          </Button>
        </ReduxForm>
      </div>
    </div>
  );
};

export default ReportInfoPage;
