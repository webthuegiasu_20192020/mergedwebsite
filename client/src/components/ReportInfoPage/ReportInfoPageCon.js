import { connect } from "react-redux";
import {
  getProtected,
  postProtected,
  mergeFormReport
} from "../../actions/actions";
import ReportInfoPage from "./ReportInfoPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    match: ownProps.match
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    data["status"] = "resolve";
    data["dateEnd"] = Date.now().toLocaleString();
    dispatch(postProtected("/admin/report/update", data));
  },
  banTutor: data => {
    let banItem = { id: data };
    console.log(data);
    dispatch(postProtected("/admin/user/ban", banItem));
  },
  onLoad: id => {
    dispatch(getProtected(`/admin/report/${id}`, mergeFormReport));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportInfoPage);
