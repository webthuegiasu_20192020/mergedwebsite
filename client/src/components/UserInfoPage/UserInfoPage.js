import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import AdminNavBar from "../AdminNavBar";
import "./UserInfoPage.css";

const UserInfoPage = ({ match, token }) => {
  const [data, setData] = useState({
    id: "",
    username: "",
    password: "",
    firstname: "",
    lastname: "",
    email: "",
    type: 3,
    address: ""
    //Detail: {
    //description: "",
    //creditCard: "",
    //}
  });
  useEffect(() => {
    const fetchData = async (match, token) => {
      fetch(`/admin/user/${match.params.id}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(setData);
    };
    fetchData(match, token);
  }, [match, token]);
  const type = type => {
    if (type === 3) {
      return "";
    } else if (type === 2) {
      return "Tutor";
    } else {
      return "Student";
    }
  };
  return (
    <div>
      <AdminNavBar />
      <div className="Content">
        <Link to="/admin/user" className="btn btn-success">
          Return to Users List
        </Link>
        <div className="user-info mt-2">
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">UserName:</p>
            {data.username}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Email:</p>
            {data.email}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">First Name:</p>
            {data.firstname}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Last Name:</p>
            {data.lastname}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Type:</p>
            {type(data.type)}
          </div>
          {/* <h5>User's Details:</h5>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Address:</p>
            {data.address}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Description:</p>
            {data.Detail.description}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Credit Card:</p>
            {data.Detail.creditCard}
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default UserInfoPage;
