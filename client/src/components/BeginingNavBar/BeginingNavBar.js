import React, { useState, useEffect } from "react";
import {Navbar, Button} from "react-bootstrap"
import {Link, Redirect} from "react-router-dom"
import "./BeginingNavBar.css";

const BeginningNavBar = ({ handleClick, id, token }) => {
  let [data, setData] = useState({
    id: "",
    username: "",
    firstname: "",
    lastname: ""
  });

  
  useEffect(() => {
    const fetchData = async (id, token) => {
      fetch(`/users/${id}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(setData);
    };
    fetchData(id, token);
  }, [id, token]);

  const [isRedirect, MakeRedirect] = useState(false);

  const LogOut = () => {
    handleClick();
    MakeRedirect(true);
  };

  if (isRedirect) {
    return <Redirect to="/login" />;
  }
  
  return (
    <div className="BeginNavBar">
      <Navbar.Brand className="SignupNavBrand">
        <Link
          to="/home"
          style={{ textDecoration: "none", color: "forestgreen" }}
        >
          Online Tutors Services
        </Link>
      </Navbar.Brand>
      {token === null ? 
      (
        <div>
          {data.username}
        <Button className="LogInBtn btn" variant="outline-success" href="/login">Log In</Button>
        </div>
      ) : 
      (
        <div>
          <Button className="LogOutBtn btn btn-light" onClick={() => LogOut()}>
            Log Out
          </Button>
        </div>
      )
      }

    </div>
  );
};

export default BeginningNavBar;
