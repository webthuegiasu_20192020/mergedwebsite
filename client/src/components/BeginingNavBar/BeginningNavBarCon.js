import { connect } from "react-redux";
import { logOut, redirectAPI } from "../../actions/actions";
import BeginingNavBar from "./BeginingNavBar";


const mapStateToProps = (state, ownProps) => {
  const { apiAction, user } = state;
  return {
    isRedirect: apiAction.isRedirect,
    token: user.token,
    id: user.data.id,
    match: ownProps.match
  };
};

const mapDispatchToProps = dispatch => ({
  handleClick: () => {
    dispatch(logOut());
    dispatch(redirectAPI("/login"));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BeginingNavBar);
