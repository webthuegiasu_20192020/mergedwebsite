import { connect } from "react-redux";
import { actions } from "react-redux-form";
import {
  postProtected,
} from "../../actions/actions";
import HireTutor from "./HireTutor";


const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    match: ownProps.match
  };
};

const mapDispatchToProps = dispatch => ({
    handleSubmit: data => {
        console.log(data);
        fetch(`/users/contract/post/tutor=${tutor_id}`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            tutor_id: data.tutor_id,
            student_id: data.student_id,
            hours: data.hours,
            condition: data.condition,
            skill: data.skill
          }),
          redirect: 'follow'
        })
          .then(res => res.json(),
                error => {console.log(error)}
          );
        dispatch(postProtected("/users/contract", data));
    },

    handleClick: model => {
    return dispatch => {
      dispatch(actions.toggle(model));
    };
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HireTutor);
