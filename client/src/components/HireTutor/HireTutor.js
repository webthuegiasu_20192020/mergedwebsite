import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import { Container, Card, Button, Form } from "react-bootstrap";
import Footer from "../Footer";

const hireTutorForm = props => {
  return <Form.Control {...props} />;
};

const HireTutor = ({ handleSubmit, handleClick, match, token }) => {
  const [isRedirect,MakeRedirect] =useState(false);
  const [data, setData] = useState({
    tutor_id: "",
    tutor_name: "",
    description: "",
    hourlyrate: "",
    address: "",
    skills: []
  });

  useEffect(() => {
    const fetchData = async (match, token) => {
      fetch(`/users/contract/post/tutor=${match.params.id}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then((data)=>{
          console.log(data)
          setData(data)
        });
    };
    fetchData(match, token);
  }, [match, token]);


  if (isRedirect) {
    return <Redirect to="/users/contract" />;
  }

  const handleClickSubmit = v => {
    handleSubmit(v);
    MakeRedirect(true);
  };

  return (
    <div>
      <BeginingNavBar></BeginingNavBar>
      <div className="popup">
        <div className="popup_inner">
          <Container>
            <Form onSubmit={event => handleClickSubmit(event)}>
              <Card>
                <Card.Header className="text-center">
                  <h1>Hire this tutor for a course</h1>
                </Card.Header>
                <Card.Body>
                    <div>Tutor: {` ` + data.tutor_name}</div>

                  
                    <Form.Label>
                      Description: {` ` + data.description}
                    </Form.Label>
                  

                    <Form.Label>Address: {` ` + data.address}</Form.Label>

                    <div>
                      Tuition Fee: {` ` + data.hourlyrate}
                    </div>

                    <Form.Label>Skill: </Form.Label>
                    <Form.Control as="select" style={{ width: "15%" }}>
                      {data.skills.map((skill,index )=> (
                        <option
                          key={index}
                          value={skill._id}
                          component={hireTutorForm}
                          model=".skill"
                          changeAction={model => handleClick(model)}
                        >
                          {skill.name}
                        </option>
                      ))}
                    </Form.Control>

                    <Form.Label>Hours:</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows="1"
                      style={{ width: "25%" }}
                      model=".hours"
                      component={hireTutorForm}
                    />

                  <Form.Label>Contract's condition from Student:</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows="3"
                      model=".condition"
                      component={hireTutorForm}
                    />

                  <div className="text-center">
                    <Button
                      className="popupBtn"
                      variant="success"
                      type="submit"
                    >
                      Hire this Tutor
                    </Button>
                    <br></br>
                    <Button
                      className="popupBtn btn"
                      variant="dark"
                      href="/listoftutors"
                    >
                      Go back
                    </Button>
                  </div>
                </Card.Body>
              </Card>
            </Form>
          </Container>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default HireTutor;
