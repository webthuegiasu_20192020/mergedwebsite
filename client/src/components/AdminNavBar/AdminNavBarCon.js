import { connect } from "react-redux";
import { logOut } from "../../actions/actions";
import AdminNavBar from "./AdminNavBar";

const mapDispatchToProps = dispatch => ({
  handleClick: () => {
    dispatch(logOut());
  }
});

export default connect(
  null,
  mapDispatchToProps
)(AdminNavBar);
