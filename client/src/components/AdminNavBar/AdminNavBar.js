import React, { useState } from "react";
import { Button, Form, Navbar, Nav } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import "./AdminNavBar.css";

const AdminNavBar = ({ handleClick }) => {
  const [isRedirect, MakeRedirect] = useState(false);

  const logOut = () => {
    handleClick();
    MakeRedirect(true);
  };

  if (isRedirect) {
    return <Redirect to="/admin/skill" />;
  }
  return (
    <Navbar bg="light" variant="light" className="BeginNavBar">
      <Navbar.Brand className="SignupNavBrand">
        <Link
          to="/home"
          style={{ textDecoration: "none", color: "forestgreen" }}
        >
          Online Tutors Services
        </Link>
      </Navbar.Brand>

      <Nav className="mr-auto">
        <Nav.Link as={Link} to="/admin">
          Admin
        </Nav.Link>
        <Nav.Link as={Link} to="/admin/user">
          Users
        </Nav.Link>
        <Nav.Link as={Link} to="/admin/skill">
          Skill
        </Nav.Link>
        <Nav.Link as={Link} to="/admin/contract">
          Contract
        </Nav.Link>
        <Nav.Link as={Link} to="/admin/report">
          Report
        </Nav.Link>
        <Nav.Link as={Link} to="/admin/stats">
          Stats
        </Nav.Link>
      </Nav>
      <Form inline className="ml-5">
        <Button
          variant="light"
          onClick={() => logOut()}
          type="button"
          className="ml-3 bg-white"
        >
          Log Out
        </Button>
      </Form>
    </Navbar>
  );
};

export default AdminNavBar;
