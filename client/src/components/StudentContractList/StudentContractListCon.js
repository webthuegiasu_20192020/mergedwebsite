import { connect } from "react-redux";
import StudentContractList from "./StudentContractList";

const mapStateToProps = (state, ownProps) => {
    const { user } = state;
    return {
      token: user.token,
      match: ownProps.match
    };
};

export default connect(
    mapStateToProps,
    null
)(StudentContractList);
  