import React, { useState, useEffect } from "react";
import BeginingNavBar from "../BeginingNavBar";
import {Container, Card, Form} from "react-bootstrap";
import ReactPaginate from "react-paginate";
import "./StudentContractList.css";
import Footer from "../Footer";

const StudentContractList = ({token, offSet}) => {
    let [dataContract, setData] = useState([{
        contract_id: "",
        tutor_id: "",
        tutor_name: "",
        description: "",
        address: "",
        skill: "",
        hourlyrate: "",
        condition: "",
        hours: "",
        status: "",
    }]);

    let [currentPage, setPage] = useState(0);
    let [pageCount, setPageCount] = useState(1);
    
    useEffect(() => {
        const fetchData = async (token, currentPage, offSet) => {
          const indexStart = currentPage * offSet;
          fetch(`/users/contract?indexStart=${indexStart}&offSet=${offSet}`, {
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
              Authorization: `Bearer ${token}`
            }
          })
            .then(res => res.json())
            .then(data => {
                setData(data.array);
                setPageCount(data.pageCount / offSet);
            });
        };
        fetchData(currentPage, offSet);
    }, [token, offSet, currentPage]);
  
    const handlePageClick = data => {
        setPage(data.selected);
        console.log(data.selected);
    };

    const contractList = dataContract.map((data, index) => {
        return (
            <Form key={index} href={`/users/contract/${data.contract_id}`}>
                    <Card>
                        <Card.Header className="text-center"><h1>Tutor Contract #{index + 1}</h1></Card.Header>
                        <Card.Body>
                                <Form inline>
                                    <Form.Label href={`/users/${data.tutor_id}`}>
                                        Tutor: {` ` + data.tutor_name}
                                    </Form.Label>
                                </Form>

                                <Form inline>
                                    <Form.Label>
                                        Address: {` ` + data.address}
                                    </Form.Label>
                                </Form>

                                <Form inline>
                                    <Form.Label>
                                        Tuition Fee: {` ` + data.hourlyrate}
                                    </Form.Label>
                                </Form>

                                <Form inline>
                                    <Form.Label>Skill: {data.skill}</Form.Label>
                                </Form>

                                <Form inline>
                                    <Form.Label>Hours: {data.hours}</Form.Label>
                                </Form>

                                <Form inline>Status:
                                    <Form.Label>{data.status}</Form.Label>
                                </Form>
                        </Card.Body>
                    </Card>
            </Form>
        );
    });

    return (
        <div>
            <BeginingNavBar></BeginingNavBar>
            <div className="contentMargin">
                <Container>
                    {contractList}
                </Container>

                <div className="d-flex justify-content-center contentMargin">
                    <ReactPaginate
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={data => handlePageClick(data)}
                    containerClassName={"pagination"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    activeClassName={"active"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    />
                </div>

                <Footer></Footer>
            </div>
        </div>
    );
}

export default StudentContractList;
