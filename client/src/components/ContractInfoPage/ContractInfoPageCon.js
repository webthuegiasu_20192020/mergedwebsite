import { connect } from "react-redux";
import { postProtected } from "../../actions/actions";
import ContractInfoPage from "./ContractInfoPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    match: ownProps.match
  };
};

const mapDispatchToProps = dispatch => ({
  deleteSkill: data => {
    let delItem = { id: data };
    console.log(data);
    dispatch(postProtected("/admin/contract/delete", delItem));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContractInfoPage);
