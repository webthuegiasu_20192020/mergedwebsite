import React, { useEffect, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import AdminNavBar from "../AdminNavBar";
import { Button } from "react-bootstrap";
import "./ContractInfoPage.css";

const ContractInfoPage = ({ match, token, deleteContract }) => {
  const [data, setData] = useState({
    id: "",
    tutor_id: "",
    tutor_name: "",
    student_id: "",
    student_name: "",
    condition: "",
    skill: "",
    hours: "",
    status: "",
    date_send: "",
    date_accept: "",
    date_finish: ""
  });
  useEffect(() => {
    const fetchData = async (match, token) => {
      fetch(`/admin/contract/${match.params.id}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(res=>{
          console.log(res);
          setData(res);
        });
    };
    fetchData(match, token);
  }, [match, token]);

  const [isRedirect, MakeRedirect] = useState(false);

  const handleClickDetele = v => {
    deleteContract(v);
    MakeRedirect(true);
  };

  if (isRedirect) {
    return <Redirect to="/admin/contract" />;
  }

  return (
    <div>
      <AdminNavBar />
      <div className="Content">
        <Link to="/admin/contract" className="btn btn-success">
          Return to Contracts List
        </Link>
        <Button
          variant="dangger"
          type="button"
          className="float-right"
          onClick={() => handleClickDetele(match.params.id)}
        >
          Detele
        </Button>
        <div className="user-info mt-2">
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Tutor name:</p>
            <Link to={`/admin/user/${data.tutor_id}`} className="btn btn-link">
              {data.tutor_name}
            </Link>
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Student name:</p>
            <Link
              to={`/admin/user/${data.student_id}`}
              className="btn btn-link"
            >
              {data.student_name}
            </Link>
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Skill:</p>
            {data.skill}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Status:</p>
            {data.status}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Hours:</p>
            {data.hours}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Condition:</p>
            {data.condition}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Date send:</p>
            {data.date_send}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Date accept:</p>
            {data.date_accept}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Date finish:</p>
            {data.date_finish}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContractInfoPage;
