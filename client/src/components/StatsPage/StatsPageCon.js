import { connect } from "react-redux";
import StatsPage from "./StatsPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    offSet: ownProps.offSet
  };
};

export default connect(mapStateToProps)(StatsPage);
