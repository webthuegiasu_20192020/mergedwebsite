import React, { useState, useEffect } from "react";
import AdminNavBar from "../AdminNavBar";
import ReactPaginate from "react-paginate";
import {
  Table,
  Spinner,
  ToggleButton,
  ToggleButtonGroup,
  Button
} from "react-bootstrap";
import "./StatsPage.css";

const StatsPage = ({ token, offSet }) => {
  let [dataAdmin, setData] = useState([{ name: "", id: "", revenue: "" }]);
  let [keys, setKeys] = useState("tutors");
  let [span, setSpan] = useState("day");
  let [isLoading, setLoading] = useState(true);
  let [refresh, setRefresh] = useState(false);
  let [currentPage, setPage] = useState(0);
  let [pageCount, setPageCount] = useState(1);
  useEffect(() => {
    const fetchData = async (currentPage, offSet, keys, span) => {
      const indexStart = currentPage * offSet;
      console.log(
        `/admin/stats?indexStart=${indexStart}&offSet=${offSet}&keys=${keys}&span=${span}`
      );
      fetch(
        `/admin/stats?indexStart=${indexStart}&offSet=${offSet}&keys=${keys}&span=${span}`,
        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then(res => res.json())
        .then(data => {
          console.log(data);
          setLoading(false);
          setData(data.array);
          setPageCount(data.pageCount / offSet);
        });
    };
    fetchData(currentPage, offSet, keys, span);
  }, [token, offSet, keys, span, currentPage, refresh]);
  const dataTable = dataAdmin.map((value, index) => {
    return (
      <tr key={index}>
        <td>{index}</td>
        <td>{value.id}</td>
        <td>{value.name}</td>
        <td>{value.total}</td>
      </tr>
    );
  });

  const handlePageClick = data => {
    setLoading(true);
    setPage(data.selected);
    console.log(data.selected);
  };

  const handleRefresh = () => {
    setRefresh(!refresh);
    setLoading(true);
  };

  return (
    <div>
      <AdminNavBar />
      <div>
        <div className="Content">
          <Button variant="info" type="button" onClick={handleRefresh}>
            Refresh
          </Button>
          <div className="mb-3">
            <ToggleButtonGroup
              type="radio"
              value={keys}
              name="keys"
              onChange={setKeys}
            >
              <ToggleButton variant="success" value={"tutors"}>
                Tutors
              </ToggleButton>
              <ToggleButton variant="success" value={"skills"}>
                Skills
              </ToggleButton>
            </ToggleButtonGroup>
            <ToggleButtonGroup
              className="float-right"
              type="radio"
              name="span"
              value={span}
              onChange={setSpan}
            >
              <ToggleButton variant="success" value={"day"}>
                Day
              </ToggleButton>
              <ToggleButton variant="success" value={"week"}>
                Week
              </ToggleButton>
              <ToggleButton variant="success" value={"30"}>
                30 Day
              </ToggleButton>
              <ToggleButton variant="success" value={"90"}>
                90 Day
              </ToggleButton>
              <ToggleButton variant="success" value={"all"}>
                All
              </ToggleButton>
            </ToggleButtonGroup>
          </div>
          <Table striped hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Revenue</th>
              </tr>
            </thead>
            <tbody>
              {isLoading ? (
                <tr>
                  <td colSpan="4" className="text-center">
                    <Spinner animation="border" variant="light" />
                  </td>
                </tr>
              ) : (
                dataTable
              )}
            </tbody>
          </Table>
          <div className="d-flex justify-content-center">
            <ReactPaginate
              previousLabel={"Previous"}
              nextLabel={"Next"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={data => handlePageClick(data)}
              containerClassName={"pagination"}
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              activeClassName={"active"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default StatsPage;
