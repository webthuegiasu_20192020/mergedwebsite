import React, { useState, useEffect } from "react";
import ReactPaginate from "react-paginate";
import { Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import {Container, Row, Col, Card, Button} from "react-bootstrap";
import "./ListOfTutors.css";
import Footer from '../Footer'

const ListOfTutorsAddress = ({offSet}) => {
    let [dataTutors, setData] = useState([{
        _id: "",
        tutor_name: "",
        description: "",
        address: "",
        skills: [],
        hourlyrate: ""
    }]);

    let [currentPage, setPage] = useState(0);
    let [pageCount, setPageCount] = useState(1);
    
    useEffect(() => {
        const fetchData = async (currentPage, offSet) => {
          const indexStart = currentPage * offSet;
          fetch(`/tutorsbyaddress?indexStart=${indexStart}&offSet=${offSet}`, {
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json"
            }
          })
            .then(res => res.json())
            .then(data => {
                setData(data.array);
                setPageCount(data.pageCount / offSet);
            });
        };
        fetchData(currentPage, offSet);
    }, [offSet, currentPage]);
  
    const handlePageClick = data => {
        setPage(data.selected);
        console.log(data.selected);
    };


    const tutorsList = dataTutors.map((data, index) => {
        return (
            <div>
                <Card border="secondary" style={{width: '50rem'}} className="listContent"  key={index} href={`/users/${data._id}`}>
                    <Card.Body>
                        <Card.Title>{data.tutor_name}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Specialty: {`,` + data.skills}</Card.Subtitle>
                        <Card.Text>
                        {data.description}
                        <br></br>
                        Address: {data.address}
                        <br></br>
                        Tuition Fee: {data.hourlyrate} /h
                        </Card.Text>
                        <Button variant="outline-success" href={`/users/contract/post/tutor=${data._id}`}>Hire Tutor</Button>
                    </Card.Body>
                </Card>
            </div>
        )
    });

    return (
        <div>
            <BeginingNavBar></BeginingNavBar>
            <div className="contentMargin">
                <Container>
                    <Row>
                        <Col xs={6} md={4} id="containerSidebar">
                            <Card bg="success" text="white" style={{width: '18rem'}}>
                                <Card.Body>
                                    <Card.Title>Category</Card.Title>
                                    <div>
                                        <Link to="/listoftutors" style={{textDecoration: "none", color: "white"}}>All Categories</Link>
                                    </div>
                                    <div>
                                        <Link to="/listoftutorsbysubject" style={{textDecoration: "none", color: "white"}}>Sort by Subjects</Link>
                                    </div>
                                    <div>
                                        <Link to="/listoftutorsbyrate" style={{textDecoration: "none", color: "white"}}>Sort by Hourly Rate</Link>
                                    </div>
                                    <div>
                                        <Link to="/listoftutorsbyaddress" style={{textDecoration: "none", color: "white"}}>Sort by Address</Link>
                                    </div>
                                </Card.Body>
                            </Card>
                        </Col>
                        
                        <Col xs={12} md={8} id="containerContent">
                            {tutorsList}
                        </Col>
                    </Row>
                    
                </Container>

                <div className="d-flex justify-content-center contentMargin">
                    <ReactPaginate
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={data => handlePageClick(data)}
                    containerClassName={"pagination"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    activeClassName={"active"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    />
                </div>

                <Footer></Footer>
            </div>
        </div>
    )
}
export default ListOfTutorsAddress;