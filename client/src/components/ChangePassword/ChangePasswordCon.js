import {connect} from 'react-redux';
import {getAllUser, redirectAPI} from '../../actions/actions';
import ChangePassword from './ChangePassword';

const mapStateToProps = state => {
  const {apiAction} = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL
  };
};

const mapDispatchToProps = dispatch => ({
    handleSubmit: data => {

        fetch('/changepassword', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            password: data.password 
          }),
          redirect: 'follow'
        })
          .then(res => res.json(),
                error => {console.log(error)}
          );
        dispatch(redirectAPI('/me'));
      }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserLogin);