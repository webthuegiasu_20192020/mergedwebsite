import React from 'react';
import {Button, Form,  Modal,Card, Row, Col} from 'react-bootstrap';
import {Link, Redirect} from 'react-router-dom';
import {Form as ReduxForm, Control} from 'react-redux-form';
import './UserDetail.css'
import BeginNavBar from '../BeginingNavBar'

const ChangePassword = ({handleSubmit, isRedirect, URL}) => {
    const link = isRedirect => {
        if (isRedirect) {
        return <Redirect to={URL}/>;
        }
        return <div/>;
    };

    return (
        <div>
            <BeginNavBar />
            <ReduxForm model='tutorProfile' onSubmit={event => handleSubmit(event)}>
                <Modal.Dialog size='xl'>
                    <Modal.Header>
                        <Modal.Title>CHANGE PASSWORD</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                    <Form.Group>
                        <Form.Label>New password</Form.Label>
                        <Control.text
                        name='password'
                        model='.password'
                        component={bootstrapForm}
                        placeholder='Enter your new password'
                        type='password'
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Confirm new password</Form.Label>
                        <Control.text
                        name='password'
                        model='.repassword'
                        component={bootstrapForm}
                        placeholder='Re enter your new password'
                        type='password'
                        />
                    </Form.Group>
                    </Modal.Body>

                    <Modal.Footer>
                    <Button className='btn btn-primary Loginbtn'  type='submit' block='true'>
                        Confirm
                    </Button>

                    <Link to='/me' className='btn Signup'>Cancel</Link>
                    </Modal.Footer>
                </Modal.Dialog>
            </ReduxForm>
        </div>
    )
}