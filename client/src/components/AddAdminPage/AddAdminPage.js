import React, { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import AdminNavBar from "../AdminNavBar";
import { Form, Button } from "react-bootstrap";
import { Form as ReduxForm, Control, Errors } from "react-redux-form";
import "./AddAdminPage.css";

const bootstrapFormControl = props => {
  return <Form.Control {...props} />;
};

const AddAdminPage = ({ handleSubmit, getUsername }) => {
  const [isRedirect, MakeRedirect] = useState(false);

  if (isRedirect) {
    return <Redirect to="/admin" />;
  }

  const handleClick = v => {
    handleSubmit(v);
    MakeRedirect(true);
  };

  let newPassword = (
    Math.floor(Math.random() * 1000000000) + 100000000
  ).toString();

  return (
    <div>
      <AdminNavBar />
      <div className="form">
        <div className="Content">
          <Link to="/admin/" className="btn btn-success">
            Return to Administration View
          </Link>
          <ReduxForm model="tempAdminUser" onSubmit={v => handleClick(v)}>
            <Form.Group controlId="formBasicCreditCard">
              <Form.Label>Username</Form.Label>
              <Control.text
                model=".username"
                component={bootstrapFormControl}
                type="text"
                disabled
              />
            </Form.Group>
            <Form.Group controlId="formBasicAddress">
              <Form.Label>Address</Form.Label>
              <Control.text
                model=".email"
                component={bootstrapFormControl}
                type="email"
                updateOn="change"
                changeAction={(model, value) => getUsername(model, value)}
                validators={{
                  isRequired: val => val && val.length
                }}
              />
              <Errors
                model="detailForm.address"
                className="alert alert-danger"
                show={field => field.touched && !field.focus}
                messages={{
                  isRequired: "Please input new administrator email",
                  isEmail: "Please input correct email formt"
                }}
              />
            </Form.Group>
            <Form.Group controlId="formBasicAddress">
              <Form.Label>Password</Form.Label>
              <Control.text
                model=".password"
                component={bootstrapFormControl}
                type="text"
                defaultValue={newPassword}
                disabled
              />
            </Form.Group>
            <div className="w-100" />
            <Button variant="primary" type="submit">
              Add
            </Button>
          </ReduxForm>
        </div>
      </div>
    </div>
  );
};

export default AddAdminPage;
