import { connect } from "react-redux";
import { actions } from "react-redux-form";
import {
  getProtected,
  postProtected,
} from "../../actions/actions";
import StudentContractDetail from "./StudentContractDetail";

const mapStateToProps = (state, ownProps) => {
    const { user } = state;
    return {
      token: user.token,
      match: ownProps.match
    };
};

const mapDispatchToProps = dispatch => ({
    handleSubmit: data => {
        console.log(data);
        fetch(`/users/contract/${data.id}`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            status: data.status
          }),
          redirect: 'follow'
        })
          .then(res => res.json(),
                error => {console.log(error)}
          );
        console.log(data);
        dispatch(postProtected("/users/contract", data));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StudentContractDetail);
  