import React, { useState, useEffect } from "react";
import BeginingNavBar from "../BeginingNavBar";
import {Container, Card, Button, Form} from "react-bootstrap";
import {Redirect} from "react-router-dom"
import "./StudentContractDetail.css";
import Footer from "../Footer";

const hireTutorForm = props => {
  return <Form.Control {...props} />;
};

const StudentContractDetail = ({handleSubmit, handleReport, match, token}) => {
    let [data, setData] = useState({
        id: "",
        tutor_id: "",
        tutor_name: "",
        description: "",
        address: "",
        skill: "",
        hourlyrate: "",
        condition: "",
        hours: "",
        status: "",
        date_send: "",
    });
    
    useEffect(() => {
        const fetchData = async (match, token) => {
          fetch(`/users/contract/${match.params.id}`, {
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
              Authorization: `Bearer ${token}`
            }
          })
            .then(res => res.json())
            .then(setData);
        };
        fetchData(match, token);
    }, [match, token]);
    
    const isRedirect = useState(false);
  
  if (isRedirect) {
    return <Redirect to="/users/contract" />;
  }

  return (
    <div>
      <BeginingNavBar></BeginingNavBar>
      <div>
        <Container>
          <Form key={data.contract_id}>
            <Card>
              <Card.Header className="text-center">
                <h1>Tutor Contract</h1>
              </Card.Header>
              <Card.Body>
                <Form inline>
                  <Form.Label href={`/users/${data.tutor_id}`}>
                    Tutor: {` ` + data.tutor_name}
                  </Form.Label>
                </Form>

                <Form inline>
                    <Form.Label>Skill: {data.skill}</Form.Label>
                </Form>

                <Form inline>
                    <Form.Label>Hours: {data.hours}</Form.Label>
                </Form>

                <Form inline>
                  <Form.Label>Description: {` ` + data.description}</Form.Label>
                </Form>

                <Form inline>
                  <Form.Label>Address: {` ` + data.address}</Form.Label>
                </Form>

                <Form inline>
                  <Form.Label>Tuition Fee: {` ` + data.hourlyrate}</Form.Label>
                </Form>

                <Form>
                  <Form.Label>Skill: {data.skill}</Form.Label>
                </Form>

                <Form>
                  <Form.Label rows="1" style={{ width: "25%" }}>
                    Hours: {data.hours}
                  </Form.Label>
                </Form>

                <Form>
                  Contract's condition from Student:
                  <Form.Label>{data.condition}</Form.Label>
                </Form>

                <Form>
                  Status:
                  <Form.Control
                    as="select"
                    style={{ width: "15%" }}
                    onChange={e => setData({ status: e })}
                    component={hireTutorForm}
                    model=".status"
                  >
                    {data.status === "Pending" && data.status != "Active" ? (
                      <div>
                        {data.status}
                        <option value="Active">Active</option>
                        <option value="Finished">Finished</option>
                      </div>
                    ) : (
                      <div>
                        {data.status}
                        <option value="Finished">Finished</option>
                      </div>
                    )}
                  </Form.Control>
                </Form>

                <div className="text-center">
                  {data.status === "Finished" ? (
                    <div>
                      <Button
                        className="popupBtn"
                        variant="success"
                        type="submit"
                        onSubmit={event => handleSubmit(event)}
                      >
                        End the Contract
                      </Button>
                      <Button
                        className="popupBtn"
                        variant="danger"
                        type="submit"
                        onSubmit={event => handleReport(event)}
                        href={`/users/contract/report/${data.contract_id}`}
                      >
                        Report the Contract
                      </Button>
                    </div>
                  ) : null}
                  <br></br>
                  <Button
                    className="popupBtn"
                    variant="dark"
                    href="/users/contract"
                  >
                    Go back
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Form>
        </Container>
        <Footer></Footer>
      </div>
    </div>
  );
};

export default StudentContractDetail;
