import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import AdminNavBar from "../AdminNavBar";
import ReactPaginate from "react-paginate";
import { Table, Spinner, Button } from "react-bootstrap";
import "./SkillPage.css";

const SkillPage = ({ token, offSet }) => {
  let [dataAdmin, setData] = useState([{ name: "", id: "" }]);
  let [isLoading, setLoading] = useState(true);
  let [refresh, setRefresh] = useState(false);
  let [currentPage, setPage] = useState(0);
  let [pageCount, setPageCount] = useState(1);
  useEffect(() => {
    const fetchData = async (currentPage, offSet) => {
      const indexStart = currentPage * offSet;
      fetch(`/admin/skill?indexStart=${indexStart}&offSet=${offSet}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(data => {
          setLoading(false);
          setData(data.array);
          setPageCount(data.pageCount / offSet);
        });
    };
    fetchData(currentPage, offSet);
  }, [token, offSet, currentPage, refresh]);
  const dataTable = dataAdmin.map((value, index) => {
    return (
      <tr key={index}>
        <td>{index}</td>
        <td>{value.name}</td>
        <td>
          <Link to={`/admin/skill/${value._id}`} className="btn btn-primary">
            Detail
          </Link>
        </td>
      </tr>
    );
  });

  const handlePageClick = data => {
    setLoading(true);
    setPage(data.selected);
    console.log(data.selected);
  };
  const handleRefresh = () => {
    setRefresh(!refresh);
    setLoading(true);
  };
  return (
    <div>
      <AdminNavBar />
      <div>
        <div className="Content">
          <Link
            to="/admin/skill/add"
            className="btn btn-success mb-3 float-right"
          >
            Add Skill
          </Link>

          <Button variant="info" type="button" onClick={handleRefresh}>
            Refresh
          </Button>
          <Table striped hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {isLoading ? (
                <tr>
                  <td colSpan="3" className="text-center">
                    <Spinner animation="border" variant="light" />
                  </td>
                </tr>
              ) : (
                dataTable
              )}
            </tbody>
          </Table>
          <div className="d-flex justify-content-center">
            <ReactPaginate
              previousLabel={"Previous"}
              nextLabel={"Next"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={data => handlePageClick(data)}
              containerClassName={"pagination"}
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              activeClassName={"active"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SkillPage;
