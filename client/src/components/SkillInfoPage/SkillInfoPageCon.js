import { connect } from "react-redux";
import {
  getProtected,
  postProtected,
  mergeFormSkill
} from "../../actions/actions";
import SkillInfoPage from "./SkillInfoPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    match: ownProps.match
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    dispatch(postProtected("/admin/skill/update", data));
  },
  deleteSkill: data => {
    let delItem = { id: data };
    console.log(data);
    dispatch(postProtected("/admin/skill/delete", delItem));
  },
  onLoad: id => {
    dispatch(getProtected(`/admin/skill/${id}`, mergeFormSkill));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SkillInfoPage);
