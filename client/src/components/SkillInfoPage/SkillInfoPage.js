import React, { useEffect, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import AdminNavBar from "../AdminNavBar";

import { Button, Form } from "react-bootstrap";
import { Form as ReduxForm, Control } from "react-redux-form";
import "./SkillInfoPage.css";

const bootstrapFormControl = props => {
  return <Form.Control {...props} />;
};

const SkillInfoPage = ({ match, onLoad, handleSubmit, deleteSkill }) => {
  useEffect(() => {
    onLoad(match.params.id);
  }, [match, onLoad]);

  const [isRedirect, MakeRedirect] = useState(false);

  const handleClick = v => {
    handleSubmit(v);
    MakeRedirect(true);
  };

  const handleClickDetele = v => {
    deleteSkill(v);
    MakeRedirect(true);
  };

  if (isRedirect) {
    return <Redirect to="/admin/skill" />;
  }

  return (
    <div>
      <AdminNavBar />
      <div className="Content">
        <Link to="/admin/skill" className="btn btn-success">
          Return to Skill List
        </Link>
        <ReduxForm model="initialStateSkill" onSubmit={v => handleClick(v)}>
          <Form.Group controlId="formBasicUsername">
            <Form.Label>Name:</Form.Label>
            <Control.text
              model=".name"
              component={bootstrapFormControl}
              type="text"
            />
          </Form.Group>
          <Button variant="primary" type="submit" className="ml-1">
            Update
          </Button>
          <Button
            variant="dangger"
            type="button"
            className="ml-1"
            onClick={() => handleClickDetele(match.params.id)}
          >
            Detele
          </Button>
        </ReduxForm>
      </div>
    </div>
  );
};

export default SkillInfoPage;
