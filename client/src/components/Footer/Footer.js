import React from 'react';
import './Footer.css';

class Footer extends React.Component{
    render()
    {
        return(
            <div>
                <footer className="text-center text-muted footer">
                    2019-2020
                    <br></br>
                    Advanced Website Development
                    <br></br>
                    Online Tutor Services
                </footer>
            </div>
        )
    }
}

export default Footer;