import React from 'react';
import {Button, Form, Navbar, Modal} from 'react-bootstrap';
import {Link, Redirect} from 'react-router-dom';
import {Form as ReduxForm, Control} from 'react-redux-form';
import './UserLogin.css'
import Footer from '../Footer'

const bootstrapForm = props => {
  return <Form.Control {...props} />;
};

const UserLogin = ({handleSubmit, isRedirect, URL}) => {
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to={URL}/>;
    }
    return <div/>;
  };

  return(
        <div>
          <div>
            <Navbar expand="lg" className="BeginNavBar">
              <Navbar.Brand className="SignupNavBrand"><Link to="/home" style={{textDecoration: "none", color: "forestgreen"}}>Online Tutors Services</Link></Navbar.Brand>
            </Navbar>
          </div>

          <div className='Login'>
            <ReduxForm model='initialStateUser' onSubmit={event => handleSubmit(event)}>
              <Modal.Dialog>
                <Modal.Header>
                  <Modal.Title>LOG IN</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <Form.Group>
                    <Form.Label>Email</Form.Label>
                    <Control.text
                      autoFocus
                      name='email'
                      model='.email'
                      component={bootstrapForm}
                      placeholder='Enter your Email'
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Control.text
                      name='password'
                      model='.password'
                      component={bootstrapForm}
                      placeholder='Enter your Password'
                      type='password'
                    />
                  </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                  <Button className='btn btn-primary Loginbtn'  type='submit' block='true'>
                    Login
                  </Button>

                  <Link to='/register' className='btn Signup'>Sign Up</Link>
                </Modal.Footer>
              </Modal.Dialog>
            </ReduxForm>

            <div>{link(isRedirect)}</div>
          </div>
          <Footer></Footer>
        </div>
    )
}

export default UserLogin;
