import {connect} from 'react-redux';
import {getAllUser, redirectAPI} from '../../actions/actions';
import UserLogin from './UserLogin';

const mapStateToProps = state => {
  const {apiAction} = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    fetch('/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        username: data.username,
        password: data.password,
        type: data.type
      }),
      redirect: 'follow'
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        dispatch(getAllUser({data: res.user, token: res.token}));
        if (res.user.type === 1)
        {
          dispatch(redirectAPI('/listoftutors'));
        }
        else if (res.user.type === 2)
        {
          dispatch(redirectAPI('/detail'));
        }
        else if (res.user.type === 0)
        {
          dispatch(redirectAPI('/admin'));
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserLogin);