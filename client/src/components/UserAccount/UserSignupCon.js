import {connect} from 'react-redux';
import { actions } from "react-redux-form";
import {redirectAPI} from '../../actions/actions';
import UserSignup from './UserSignup';

const mapStateToProps = state => {
  const {apiAction} = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    fetch('/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        username: data.username,
        password: data.password,
        usertype: data.usertype 
      }),
      redirect: 'follow'
    })
      .then(res => res.json(),
            error => {console.log(error)}
      );
    dispatch(redirectAPI('/login'));
  },

  handleClick: model => {
    return dispatch => {
      dispatch(actions.toggle(model));
    };
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSignup);