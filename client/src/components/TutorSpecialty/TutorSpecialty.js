import React from "react";
import { Redirect } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import { Form, Button } from "react-bootstrap";
import { Form as ReduxForm, Control } from "react-redux-form";
import "./TutorSpecialty.css";

const bootstrapFormControl = props => {
  return <Form.Check {...props} />;
};

const tutorSpecialty = ({ handleSubmit, isRedirect, URL, handleClick }) => {
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to={URL} />;
    }
    return null;
  };
  return (
    <div>
      <BeginingNavBar />
      <div className="form">
        <div className="Content">
          <ReduxForm model="tutorSpecialty" onSubmit={v => handleSubmit(v)}>
            <div className="d-flex flex-row justify-content-around">
              <Form.Group>
                <Form.Label>Grade</Form.Label>
                <Control.checkbox
                  model=".grade.grade_1"
                  component={bootstrapFormControl}
                  label="Grade 1"
                  type="checkbox"
                  id="Grade_1"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_2"
                  component={bootstrapFormControl}
                  label="Grade 2"
                  type="checkbox"
                  id="Grade_2"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_3"
                  component={bootstrapFormControl}
                  label="Grade 3"
                  type="checkbox"
                  id="Grade_3"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_4"
                  component={bootstrapFormControl}
                  label="Grade 4"
                  type="checkbox"
                  id="Grade_4"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_5"
                  component={bootstrapFormControl}
                  label="Grade 5"
                  type="checkbox"
                  id="Grade_5"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_6"
                  component={bootstrapFormControl}
                  label="Grade 6"
                  type="checkbox"
                  id="Grade_6"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_7"
                  component={bootstrapFormControl}
                  label="Grade 7"
                  type="checkbox"
                  id="Grade_7"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_8"
                  component={bootstrapFormControl}
                  label="Grade 8"
                  type="checkbox"
                  id="Grade_8"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_9"
                  component={bootstrapFormControl}
                  label="Grade 9"
                  type="checkbox"
                  id="Grade_9"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_10"
                  component={bootstrapFormControl}
                  label="Grade 10"
                  type="checkbox"
                  id="Grade_10"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_11"
                  component={bootstrapFormControl}
                  label="Grade 11"
                  type="checkbox"
                  id="Grade_11"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".grade.grade_12"
                  component={bootstrapFormControl}
                  label="Grade 12"
                  type="checkbox"
                  id="Grade_12"
                  changeAction={model => handleClick(model)}
                />
              </Form.Group>
              <Form.Group controlId="formBasicSubject">
                <Form.Label>Subject</Form.Label>
                <Control.checkbox
                  model=".subject.math"
                  component={bootstrapFormControl}
                  label="Math"
                  type="checkbox"
                  id="Math"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".subject.literature"
                  component={bootstrapFormControl}
                  label="Literature"
                  type="checkbox"
                  id="literature"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".subject.chemistry"
                  component={bootstrapFormControl}
                  label="Chemistry"
                  type="checkbox"
                  id="chemistry"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".subject.physics"
                  component={bootstrapFormControl}
                  label="Physics"
                  type="checkbox"
                  id="physics"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".subject.biology"
                  component={bootstrapFormControl}
                  label="Biology"
                  type="checkbox"
                  id="biology"
                  changeAction={model => handleClick(model)}
                />
                <Control.checkbox
                  model=".subject.english"
                  component={bootstrapFormControl}
                  label="English"
                  type="checkbox"
                  id="english"
                  changeAction={model => handleClick(model)}
                />
              </Form.Group>
            </div>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </ReduxForm>
        </div>
      </div>
      {link(isRedirect)}
    </div>
  );
};

export default tutorSpecialty;
