import { connect } from "react-redux";
import ReportPage from "./ReportPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    offSet: ownProps.offSet
  };
};

export default connect(mapStateToProps)(ReportPage);
