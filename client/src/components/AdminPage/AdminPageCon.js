import { connect } from "react-redux";
import AdminPage from "./AdminPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    offSet: ownProps.offSet
  };
};

export default connect(mapStateToProps)(AdminPage);
