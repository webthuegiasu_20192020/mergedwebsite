import React, { useState } from "react";
import { Button, Form, Modal, Card, Row, Col } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { Form as ReduxForm, Control } from "react-redux-form";
import "./UserDetail.css";
import BeginNavBar from "../BeginingNavBar";
//import {Image} from 'cloudinary-react'

const bootstrapForm = props => {
  return <Form.Control {...props} />;
};

const UserDetail = ({ handleSubmit, isRedirect, URL }) => {
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to={URL} />;
    }
    return <div />;
  };
  const [image, setImage] = useState("");
  const [loading, setLoading] = useState(false);

  const uploadImage = async e => {
    const files = e.target.files;
    const data = new FormData();
    data.append("file", files[0]);
    data.append("upload_preset", "wfld8izf");
    setLoading(true);
    const res = await fetch(
      "	https://api.cloudinary.com/v1_1/grabteacherimage/image/upload",
      {
        method: "POST",
        body: data
      }
    );
    const file = await res.json();

    setImage(file.secure_url);
    setLoading(false);
  };

  return (
    <div>
      <BeginNavBar />
      <div>
        <div>
          <ReduxForm
            model="tutorProfile"
            onSubmit={event => handleSubmit(event)}
          >
            <Modal.Dialog size="xl">
              <Modal.Header>
                <Modal.Title>PROFILE</Modal.Title>
              </Modal.Header>

              <Modal.Body>
                <Row>
                  <Col sm={3}>
                    <Card style={{ width: "200px" }}>
                      <Card.Img alt="Profile picture" height="200px">
                        {image}
                      </Card.Img>
                      <Control.file
                        model=".img"
                        placeholder="Upload an image"
                        onChange={uploadImage}
                        component={bootstrapForm}
                      />
                    </Card>
                  </Col>
                  <Col>
                    <Row>
                      <Col>
                        <Form.Group>
                          <Form.Label>First Name</Form.Label>
                          <Control.text
                            name="firstname"
                            model=".firstname"
                            component={bootstrapForm}
                          />
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group>
                          <Form.Label>Last Name</Form.Label>
                          <Control.text
                            name="lastname"
                            model=".lastname"
                            component={bootstrapForm}
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Form.Group>
                      <Form.Label>Address</Form.Label>
                      <Control.text
                        name="address"
                        model=".address"
                        component={bootstrapForm}
                      />
                    </Form.Group>

                    <Form.Group>
                      <Control.text
                        name="tag"
                        model=".tag"
                        component={bootstrapForm}
                      />

                      <Button className="addTags" onClick={() => true}>
                        Add tags
                      </Button>
                    </Form.Group>

                    <Form.Group>
                      <Form.Label>Introduction</Form.Label>
                      <Control.text
                        name="introduction"
                        model=".introduction"
                        component={bootstrapForm}
                        rowSpan="10"
                        cols="50"
                        style={{ height: "100px" }}
                      />
                    </Form.Group>
                  </Col>
                </Row>
              </Modal.Body>

              <Modal.Footer>
                <Button
                  className="btn btn-primary changeInfo"
                  type="submit"
                  block
                >
                  Change Info
                </Button>
                <Link to="/me" className="btn Cancel">
                  Discard Change
                </Link>
                <Link to="/changepassword" className="btn changePassword">
                  Change Password
                </Link>
              </Modal.Footer>
            </Modal.Dialog>
          </ReduxForm>
          <div>{link(isRedirect)}</div>
        </div>
      </div>
    </div>
  );
};

export default UserDetail;
