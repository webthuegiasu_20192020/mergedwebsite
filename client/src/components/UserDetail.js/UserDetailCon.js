import {connect} from 'react-redux';
import { actions } from "react-redux-form";
import {redirectAPI} from '../../actions/actions';
import UserDetail from './UserDetail';

const mapStateToProps = state => {
  const {apiAction} = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    fetch('users/changeprofile', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: data.id,
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        username: data.username,
      }),
      redirect: 'follow'
    })
      .then(res => res.json(),
            error => {console.log(error)}
      );
    dispatch(redirectAPI('/user/me'));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDetail);