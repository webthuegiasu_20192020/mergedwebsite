import { connect } from "react-redux";
import { postProtected } from "../../actions/actions";
import AddAdminPage from "./AddSkillPage";

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    dispatch(postProtected("/admin/skill/add", data));
  }
});

export default connect(null, mapDispatchToProps)(AddAdminPage);
