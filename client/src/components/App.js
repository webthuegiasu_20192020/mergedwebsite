import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import UserSignup from "./UserAccount/UserSignupCon";
import UserLogin from "./UserAccount/UserLoginCon";
import UserListPage from "./UserListPage";
import UserInfoPage from "./UserInfoPage";
import ContractList from "./ContractList";
import ContractInfoPage from "./ContractInfoPage";
import ReportPage from "./ReportPage";
import ReportInfoPage from "./ReportInfoPage";
import StatsPage from "./StatsPage";
import SkillPage from "./SkillPage";
import AddSkillPage from "./AddSkillPage";
import SkillInfoPage from "./SkillInfoPage";
import DetailsPage from "./DetailsPage";
import TutorSpecialty from "./TutorSpecialty";
import ListOfTutors from "./ListOfTutors";
import ListOfTutorsRate from "./ListOfTutorsRate";
import ListOfTutorsSubject from "./ListOfTutorsSubject";
import ListOfTutorsAddress from "./ListOfTutorsAddress";
import AdminPage from "./AdminPage";
import AddAdminPage from "./AddAdminPage";
import HomePage from "./HomePage";
import HireTutor from "./HireTutor";
import StudentContractList from "./StudentContractList";
import StudentContractDetail from "./StudentContractDetail";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import UserDetail from "./UserDetail.js/UserDetail";

function App() {
  return (
    <Router>
      <Switch>
        {/* Basic User functions (Does not require logging in) */}
        <Route exact path="/" component={HomePage}></Route>
        <Route path="/home" component={HomePage}></Route>
        <Route path="/register" component={UserSignup}></Route>
        <Route path="/login" component={UserLogin}></Route>
        <Route path="/listoftutors">
          <ListOfTutors offSet="5"/>
        </Route>
        <Route path="/listoftutorsbyrate" component={ListOfTutorsRate}></Route>
        <Route
          path="/listoftutorsbysubject"
          component={ListOfTutorsSubject}
        ></Route>
        <Route
          path="/listoftutorsbyaddress"
          component={ListOfTutorsAddress}
        ></Route>

        {/* Admin functions */}
        <Route exact path="/admin">
          <AdminPage offSet="25" />
        </Route>
        <Route exact path="/admin/add" component={AddAdminPage}></Route>
        <Route exact path="/admin/skill/add" component={AddSkillPage}></Route>
        <Route exact path="/admin/skill">
          <SkillPage offSet="5" />
        </Route>
        <Route exact path="/admin/skill/:id" component={SkillInfoPage}></Route>
        <Route exact path="/admin/user">
          <UserListPage offSet="25" />
        </Route>
        <Route exact path="/admin/user/:id" component={UserInfoPage}></Route>
        <Route exact path="/admin/contract">
          <ContractList offSet="25" />
        </Route>
        <Route
          exact
          path="/admin/contract/:id"
          component={ContractInfoPage}
        ></Route>
        <Route exact path="/admin/stats">
          <StatsPage offSet="25" />
        </Route>
        <Route exact path="/admin/report">
          <ReportPage offSet="25" />
        </Route>
        <Route
          exact
          path="/admin/report/:id"
          component={ReportInfoPage}
        ></Route>
        <Route exact path="/admin/skill/:id" component={SkillInfoPage}></Route>

        {/* Tutor functions */}
        <Route path="/specialty" component={TutorSpecialty}></Route>
        <Route path="/detail" component={DetailsPage}></Route>
        <Route path='/me' component={UserDetail}></Route>
        
        {/* Student functions */}
        <Route
          exact
          path="/users/contract/post/tutor=:id"
          component={HireTutor}
        ></Route>
        <Route
          exact
          path="/users/contract"
        >
          <StudentContractList offSet='5'/>
        </Route>
        <Route
          exact
          path="/users/contract/:id"
          component={StudentContractDetail}
        ></Route>
      </Switch>
    </Router>
  );
}

export default App;
