import React from "react";
import { Link } from "react-router-dom";
import "./ContractDetail.css";

const ContractDetail = ({ data }) => {
  return (
    <div>
      <div className="ContentDetail">
        <h5>Contract Info:</h5>
        <div className="user-info mt-2">
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Tutor name:</p>
            <Link to={`/admin/user/${data.tutor_id}`} className="btn btn-link">
              {data.tutor_name}
            </Link>
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Student name:</p>
            <Link
              to={`/admin/user/${data.student_id}`}
              className="btn btn-link"
            >
              {data.student_name}
            </Link>
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Skill:</p>
            {data.skill}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Status:</p>
            {data.status}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Hours:</p>
            {data.hours}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Condition:</p>
            {data.condition}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Date send:</p>
            {data.date_send}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Date accept:</p>
            {data.date_accept}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Date finish:</p>
            {data.date_finish}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContractDetail;
