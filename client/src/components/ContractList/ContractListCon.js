import { connect } from "react-redux";
import ContractList from "./ContractList";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    offSet: ownProps.offSet
  };
};

export default connect(mapStateToProps)(ContractList);
