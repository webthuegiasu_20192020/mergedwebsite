import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import AdminNavBar from "../AdminNavBar";
import ReactPaginate from "react-paginate";
import { Table, Spinner, Button } from "react-bootstrap";
import "./ContractList.css";

const ContractList = ({ token, offSet }) => {
  let [dataAdmin, setData] = useState([
    { tutor_name: "", student_name: "", id: "", status: "" }
  ]);
  let [isLoading, setLoading] = useState(true);
  let [refresh, setRefresh] = useState(false);
  let [currentPage, setPage] = useState(0);
  let [pageCount, setPageCount] = useState(1);
  useEffect(() => {
    const fetchData = async (currentPage, offSet) => {
      const indexStart = currentPage * offSet;
      fetch(`/admin/contract?indexStart=${indexStart}&offSet=${offSet}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          setLoading(false);
          setData(data.array);
          setPageCount(data.pageCount / offSet);
        });
    };
    fetchData(currentPage, offSet);
  }, [token, offSet, currentPage, refresh]);
  const dataTable = dataAdmin.map((value, index) => {
    return (
      <tr key={index}>
        <td>{index}</td>
        <td>{value.tutor_name}</td>
        <td>{value.student_name}</td>
        <td>{value.status}</td>
        <td>
          <Link to={`/admin/contract/${value._id}`} className="btn btn-info">
            More
          </Link>
        </td>
      </tr>
    );
  });

  const handlePageClick = data => {
    setLoading(true);
    setPage(data.selected);
    console.log(data.selected);
  };

  const handleRefresh = () => {
    setRefresh(!refresh);
    setLoading(true);
  };

  return (
    <div>
      <AdminNavBar />
      <div>
        <div className="Content">
        <Button variant="info" type="button" onClick={handleRefresh}>
            Refresh
          </Button>
          <Table striped hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Tutor</th>
                <th>Student</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {isLoading ? (
                <tr>
                  <td colSpan="5" className="text-center">
                    <Spinner animation="border" variant="light" />
                  </td>
                </tr>
              ) : (
                dataTable
              )}
            </tbody>
          </Table>
          <div className="d-flex justify-content-center">
            <ReactPaginate
              previousLabel={"Previous"}
              nextLabel={"Next"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={data => handlePageClick(data)}
              containerClassName={"pagination"}
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              activeClassName={"active"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContractList;
