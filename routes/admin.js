var express = require("express");
var router = express.Router();
let passport = require("../middleware/passport");
let User = require("../models/user");
let bcrypt = require("bcrypt");
let jwt = require("jsonwebtoken");
let Skill = require("../models/skill");
let Level = require("../models/level");
let Contract = require("../models/contract");
let Report = require("../models/report");
let mongoose = require("mongoose");

//register function (CHECKED)
router.post("/add", async (req, res) => {
  User.findOne({ email: req.body.email }, async (err, result) => {
    if (result) return res.json(`This email has been taken`);
    const hashpassword = bcrypt.hashSync(req.body.password, 10);
    const user = new User({
      username: req.body.username,
      password: hashpassword,
      email: req.body.email,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      country: req.body.country,
      type: 0
    });
    try {
      const savedUser = await user.save();
      res.json(savedUser);
    } catch {
      res.status(400).json(err);
    }
  });
});

//login (CHECKED)
router.post("/login", async (req, res, next) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (user) {
      console.log(user.password);
      console.log(bcrypt.hashSync(req.body.password, 10));
      console.log(bcrypt.compareSync(req.body.password, user.password));
      if (bcrypt.compareSync(req.body.password, user.password)) {
        let token = jwt.sign(
          {
            exp: Math.floor(Date.now() / 1000) + 12 * 60 * 60,
            sub: user.email
          },
          process.env.JWT_SECRET
        );
        return res.json(token);
      } else {
        return res.status(403).json("Wrong username or password");
      }
    } else {
      return res.status(403).json("Wrong username or password");
    }
  });
});

// get admin list (CHECKED)
router.get("/", (req, res, next) => {
  let offSet = req.query.offSet;
  let indexStart = req.query.indexStart;
  User.find({ type: 0, email: { $ne: "root" } }, (err, admin) => {
    console.log(err);
    console.log(admin);
    if (err) {
      return res.json(err);
    }
    if (!admin) return res.json("No data");
    else {
      let list = admin.slice(
        indexStart,
        parseInt(offSet) + parseInt(indexStart)
      );
      return res.json({ array: list, pageCount: list.length });
    }
  });
});

//get specific user by id (CHECKED)
router.get("/user/:_id", (req, res, next) => {
  let id = req.params._id;
  console.log(id);
  User.findOne({ _id: id }, (err, user) => {
    if (err) return res.json(err);
    if (!user) return res.json("No data");
    else return res.json(user);
  });
});

//get user list (CHECKED)
router.get("/user", (req, res, next) => {
  let offSet = req.query.offSet;
  let indexStart = req.query.indexStart;
  User.find({ type: { $ne: 0 } }, (err, user) => {
    if (err) return res.json(err);
    if (!user) return res.json("No data");
    else {
      let list = user.slice(
        indexStart,
        parseInt(offSet) + parseInt(indexStart)
      );
      return res.json({ array: list, pageCount: user.length });
    }
  });
});

//add skill (CHECKED)
router.post("/skill/add", (req, res, next) => {
  Skill.find({ name: req.body.name }, async (err, skill) => {
    if (err) return res.json(err);
    console.log(skill);
    if (!skill) return res.json("This skill is already existed");
    else {
      let skill = new Skill({
        name: req.body.name
      });
      try {
        const savedSkill = await skill.save();
        res.json(savedSkill);
      } catch {
        res.status(400).json(err);
      }
    }
  });
});

//add level (CHECKED)
router.post("/level/add", (req, res, next) => {
  Level.find({ name: req.body.name }, async (err, level) => {
    if (!level) return res.json("This skill is already existed");
    else {
      let level = new Level({
        name: req.body.name
      });
      try {
        const savedLevel = await level.save();
        res.json(savedLevel);
      } catch {
        res.status(400).json(err);
      }
    }
  });
});

router.get("/skill", (req, res, next) => {
  let offSet = parseInt(req.query.offSet);
  let indexStart = parseInt(req.query.indexStart);
  Skill.find((err, skill) => {
    if (err) return res.json(err);
    if (!skill) return res.json("No data");
    else
      return res.json({
        array: skill.slice(indexStart, indexStart + offSet),
        pageCount: skill.length
      });
  });
});

//detele skill (CHECKED)
router.post("/skill/delete", (req, res, next) => {
  Skill.findOneAndDelete({ _id: req.body.id }, err => {
    if (err) return res.json(err);
    return res.json("Deleted");
  });
});

//UPDATE SKILL(CHECKED)
router.post("/skill/update", (req, res, next) => {
  Skill.findOneAndUpdate(
    { _id: req.body._id },
    { name: req.body.name },
    err => {
      if (err) return res.json(err);
      return res.json("Updated");
    }
  );
});

router.get("/skill/:_id", (req, res, next) => {
  let id = req.params._id;
  Skill.findOne({ _id: id }, (err, skill) => {
    if (err) return res.json(err);
    if (!skill) return res.json("No data");
    else return res.json(skill);
  });
});

//Change status of one user
router.post("/ban", (req, res, next) => {
  User.findOne({ _id: req.id }, (err, user) => {
    if (err) return res.json(err);
    if (!user) return res.json("Invalid user");
    user.status = "ban";
    return res.json(user.save());
  });
});

router.get("/contract", (req, res, next) => {
  let offSet = parseInt(req.query.offSet);
  let indexStart = parseInt(req.query.indexStart);
  Contract.aggregate([
    {
      $project: {
        student_id: 1,
        tutor_id: 1,
        status: 1
      }
    },
    {
      $lookup: {
        from: "users",
        localField: "tutor_id",
        foreignField: "_id",
        as: "tutor"
      }
    },
    {
      $replaceRoot: {
        newRoot: {
          $mergeObjects: [
            {
              $arrayElemAt: ["$tutor", 0]
            },
            "$$ROOT"
          ]
        }
      }
    },
    {
      $project: {
        tutor_name: "$username",
        student_id: 1,
        status: 1
      }
    },
    {
      $lookup: {
        from: "users",
        localField: "student_id",
        foreignField: "_id",
        as: "student"
      }
    },
    {
      $replaceRoot: {
        newRoot: {
          $mergeObjects: [
            {
              $arrayElemAt: ["$student", 0]
            },
            "$$ROOT"
          ]
        }
      }
    },
    {
      $project: {
        tutor_name: 1,
        student_name: "$username",
        status: 1
      }
    }
  ]).then(contract => {
    let list = contract.slice(indexStart, indexStart + offSet);
    return res.json({ array: list, pageCount: contract.length });
  });
});

router.get("/contract/:id", (req, res, next) => {
  console.log(req.params.id);
  Contract.aggregate([
    { $match: { _id: new mongoose.Types.ObjectId(req.params.id) } },
    {
      $lookup: {
        from: "users",
        localField: "tutor_id",
        foreignField: "_id",
        as: "tutor"
      }
    },
    {
      $replaceRoot: {
        newRoot: {
          $mergeObjects: [
            {
              $arrayElemAt: ["$tutor", 0]
            },
            "$$ROOT"
          ]
        }
      }
    },
    {
      $project: {
        tutor_id: 1,
        tutor_name: "$username",
        student_id: 1,
        student_name: 1,
        condition: 1,
        skill: 1,
        hours: 1,
        status: 1,
        date_send: 1,
        date_accept: 1,
        date_finish: 1
      }
    },
    {
      $lookup: {
        from: "users",
        localField: "student_id",
        foreignField: "_id",
        as: "student"
      }
    },
    {
      $replaceRoot: {
        newRoot: {
          $mergeObjects: [
            {
              $arrayElemAt: ["$student", 0]
            },
            "$$ROOT"
          ]
        }
      }
    },
    {
      $project: {
        tutor_id: 1,
        tutor_name: 1,
        student_id: 1,
        student_name: "$username",
        condition: 1,
        skill: 1,
        hours: 1,
        status: 1,
        date_send: 1,
        date_accept: 1,
        date_finish: 1
      }
    }
  ]).then(contract => {
    console.log(contract);
    res.json(contract[0]);
  });
});

router.post("/contract/delete", (req, res, next) => {
  Contract.findOneAndDelete({ _id: req.body.id }, err => {
    if (err) return res.json(err);
    return res.json("Deleted");
  });
});

//REPORT FUNCTION
router.get("/report", (req, res, next) => {
  offSet = req.query.offSet;
  indexStart = req.query.indexStart;
  Report.find((err, report) => {
    if (err) return res.json(err);
    if (!report) return res.json("No Report");
    else {
      let list = report.slice(
        indexStart,
        parseInt(indexStart) + parseInt(offSet)
      );
      return res.json({ array: list, pageCount: report.length });
    }
  });
});

router.get("/report/:id", (req, res, next) => {
  Report.find({ _id: req.params.id }, (err, report) => {
    if (err) return res.json(err);
    if (!report) return res.json("No report");
    else return res.json(report);
  });
});

router.post("/report/delete", (req, res, next) => {
  Report.findOneAndDelete({ _id: req.body.id }, err => {
    if (err) return res.json(err);
    return res.json("Deleted");
  });
});

router.get("/report/:id", (req, res, next) => {
  Report.find({ _id: req.params.id }, (err, report) => {
    if (err) return res.json(err);
    if (!report) return res.json("No report");
    else return res.json(report);
  });
});

router.post("/report/delete", (req, res, next) => {
  Report.findOneAndDelete({ _id: req.body.id }, err => {
    if (err) return res.json(err);
    return res.json("Deleted");
  });
});

//STAT
router.get("/stats", (req, res, next) => {
  offSet = req.query.offSet;
  indexStart = req.query.indexStart;
  let span = req.query.span;
  let keys = req.query.keys;

  switch (span) {
    case "day":
      realspan = 1;
      break;
    case "week":
      realspan = 7;
      break;
    case "30":
      realspan = 30;
      break;
    case "90":
      realspan = 90;
      break;
    case "all":
      realspan = null;
      break;
  }

  switch (keys) {
    case "tutors":
      Contract.aggregate([
        {
          $project: {
            tutor_id: 1,
            skill_id: 1,
            hourlyrate: 1
          }
        },
        {
          $group: {
            _id: "$tutor_id",
            total: {
              $sum: "$hourlyrate"
            }
          }
        },
        {
          $lookup: {
            from: "users",
            localField: "_id",
            foreignField: "_id",
            as: "tutor"
          }
        },
        {
          $replaceRoot: {
            newRoot: {
              $mergeObjects: [
                {
                  $arrayElemAt: ["$tutor", 0]
                },
                "$$ROOT"
              ]
            }
          }
        },
        {
          $project: {
            name: "$username",
            total: 1
          }
        },
        {
          $sort: {
            total: -1
          }
        }
      ]).then(stats =>
        res.json({
          array: stats.slice(
            indexStart,
            parseInt(indexStart) + parseInt(offSet)
          ),
          pageCount: stats.length
        })
      );
      break;
    case "skills":
      Contract.aggregate([
        {
          $project: {
            tutor_id: 1,
            skill_id: 1,
            hourlyrate: 1
          }
        },
        {
          $unwind: {
            path: "$skill_id"
          }
        },
        {
          $group: {
            _id: "$skill_id",
            total: {
              $sum: "$hourlyrate"
            }
          }
        },
        {
          $lookup: {
            from: "skills",
            localField: "_id",
            foreignField: "_id",
            as: "skill"
          }
        },
        {
          $replaceRoot: {
            newRoot: {
              $mergeObjects: [
                {
                  $arrayElemAt: ["$skill", 0]
                },
                "$$ROOT"
              ]
            }
          }
        },
        {
          $project: {
            name: 1,
            total: 1
          }
        },
        {
          $sort: {
            total: -1
          }
        }
      ]).then(stats =>
        res.json({
          array: stats.slice(
            indexStart,
            parseInt(indexStart) + parseInt(offSet)
          ),
          pageCount: stats.length
        })
      );
      break;
  }
});

module.exports = router;
