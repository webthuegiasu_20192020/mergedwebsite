var express = require('express');
var router = express.Router();
let Skill = require('../models/skill');
let Level = require('../models/level');
let User_detail = require('../models/user_detail')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json('OK');
});

// get skill list
router.get('/tutorspeciality', (req,res,next)=>{
  Skill.find((err,skill)=>{
    if(err) return res.json(err);
    if(!skill) return res.json('No speciality');
    else return res.json(skill);
  })
})

// get level list
router.get('/level', (req,res,next)=>{
  Level.find((err,level)=>{
    if(err) return res.json(err);
    if(!level) return res.json('No level');
    else return res.json(level);
  })
})



//get /detail
router.get('/detail', (req,res,next)=>{
  User_detail.findOne({idUser: req.id},(err,detail)=>{
    if(err) return res.json(err)
    return res.json(detail)
  })
})

//find tutor by subject
router.get('/tutorsbysubject', (req,res,next)=>{
  User.find({type: 2}, (err,user)=>{
      if(user) return user
  }).then(user=>{
      if(user)
      user.forEach(u=>{
          Skill.find({idUser: u._id},{name:1}, (err, result)=>{
              u.push(result)
          })            
      })
      user.sort((a,b)=>{
          if(a<b) return -1
          if(a>b) return 1
          return 0
      })
      return res.json(user)
  })
})

//find tutor by rate
router.get('/tutorsbyrate', (req,res,next)=>{
  User.find({type: 2}, (err,user)=>{
      if(user) return user
  }).then(user=>{
      if(user)
      user.forEach(u=>{
          User_detail.find({idUser: u._id},{price:1}, (err, result)=>{
              u.push(result)
          })            
      })
      user.sort((a,b)=>{
          if(a<b) return -1
          if(a>b) return 1
          return 0
      })
      return res.json(user)
  })
})

//find tutor by address
router.get('/tutorsbyaddress', (req,res,next)=>{
  User.find({type: 2}, (err,user)=>{
      if(user) return user
  }).then(user=>{
      if(user)
      user.forEach(u=>{
          User_detail.find({idUser: u._id},{address:1}, (err, result)=>{
              u.push(result)
          })            
      })
      user.sort((a,b)=>{
          if(a<b) return -1
          if(a>b) return 1
          return 0
      })
      return res.json(user)
  })
})

//get toptutor list
router.get('/toptutors',(req,res,next)=>{
  User.aggregate([
    {
      '$match': {
        'type': 2
      }
    }, {
      '$lookup': {
        'from': 'contracts', 
        'localField': '_id', 
        'foreignField': 'tutor_id', 
        'as': 'contract'
      }
    }, {
      '$replaceRoot': {
        'newRoot': {
          '$mergeObjects': [
            {
              '$arrayElemAt': [
                '$contract', 0
              ]
            }, '$$ROOT'
          ]
        }
      }
    }, {
      '$project': {
        'username': 1, 
        'firstname': 1, 
        'lastname': 1, 
        'skill': 1, 
        'hourlyrate': 1
      }
    }, {
      '$sort': {
        'hourlyrate': 1
      }
    }
  ]).then(tutor=>res.json(tutor))
})


module.exports = router;
