var express = require('express');
var router = express.Router();
let passport = require("../middleware/passport")
let User= require('../models/user')
let User_detail = require('../models/user_detail')
let Contract = require('../models/contract')
let Skill = require('../models/skill')
let bcrypt = require('bcrypt')
let jwt= require('jsonwebtoken')
let mongoose= require("mongoose")
//require('../config/cloudinaryConfig')

//register function (CHECKED)
router.post('/register', async (req,res)=>{
	User.findOne({ email: req.body.email},async (err,result)=>{
		if(result) return res.json(`This email has been taken`)
	let {password, repassword} = req.body
	/*if(password.trim().length !==0 && password!==repassword)
	  return res.json('Incorrect repassword')*/
	const hashpassword=bcrypt.hashSync(password,10);
	let user;
	user = new User({
		username: req.body.username,
		password: hashpassword,
		email: req.body.email,
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		country: req.body.country,
		type: req.body.type
	});
	
  	try{
		const savedUser = await user.save();
		res.json(savedUser);
  	}catch{
 		res.status(400).json(err);
	  }
	}
)})

//login (CHECKED)
router.post("/login", async(req, res, next) => {
	User.findOne({ email: req.body.email},(err,user)=>{
	if (user) {
		if (bcrypt.compareSync(req.body.password,user.password)) {
			let token = jwt.sign(
				{
					exp: Math.floor(Date.now() / 1000) + 12 * 60 * 60,
					sub: user.email
				},
				process.env.JWT_SECRET
			);
			return res.json({token:token, user: user});
		} else {
			return res.status(403).json("Wrong username or password")
		}
	} else {
		return res.status(403).json("Wrong username or password")
	}
  });
});

// change profile (CHECKED)
router.post('/changeprofile', async (req,res)=>{
	const userDetail = {
		description: req.body.description,
		address: req.body.address,
		creditCard: req.body.creditCard,
		tag: req.body.tag,
		price: req.body.price
	}
	User_detail.findOneAndUpdate({idUser: req.body.idUser}, userDetail, async (err,doc)=>{
		if(err)  return res.json(err)
		if(!doc)
			try{
				const detail = new User_detail({idUser: req.body.idUser,...userDetail})
				const save = await detail.save()
				return res.json(save)
			}catch(e){
				return res.status(400).json(e);
			}
		else{
			return res.json(doc)
		}
	}
)})

//router get this user (CHECKED)
router.get('user/?offSet=value&indexStart=value', (req,res,next)=>{
	User.findOne({_id: req.body.id}, (err, user)=>{
		if (err) return res.json(err)
		if(!user) return res.json("Something wrong")
		else{
			User_detail.findOne({idUser: user._id},{_id:0, idUser:0, __v:0},(err,detail)=>{
				if(err) return res.json(err)				
				return res.json({...user.toObject(),...detail.toObject()})
			})
		}
	})
})


//changepassword of the current user
router.post('/changepassword',(req,res,next)=>{
		User.findOneAndUpdate({_id: req.body.id},{password: req.body.password}, (err,user)=>{
			if(err) return res.json(err)
			else return res.status(200).json("Change password success")
		})
	}
)

router.post('/changepicture', (req,res,next)=>{
	User.findOne({_id: req.body.id}, (err,user)=>{
		if(err) return res.json(err)
		if(!user) return res.json("No user")
		else {
			User_detail.findOneAndUpdate({idUser: user._id}, {img: req.body.img}, (err,detail)=>{
				if(err) return res.json(err)
				return res.status(200).json("Change image success")
			})
		}
	})
	
})

//CONTRACT FUNCTION

// get contract list
router.get('/contract', (req,res,next)=>{
	Contract.aggregate([
		{
		  '$lookup': {
			'from': 'users', 
			'localField': 'tutor_id', 
			'foreignField': '_id', 
			'as': 'tutor'
		  }
		}, {
		  '$replaceRoot': {
			'newRoot': {
			  '$mergeObjects': [
				{
				  '$arrayElemAt': [
					'$tutor', 0
				  ]
				}, '$$ROOT'
			  ]
			}
		  }
		}, {
		  '$project': {
			'_id': 1, 
			'tutor_id': 1, 
			'student_id': 1, 
			'tutor_firstname': '$firstname', 
			'tutor_lastname': '$lastname', 
			'description': 1, 
			'address': 1, 
			'hourlyrate': 1, 
			'hours': 1, 
			'skill_id': 1, 
			'skillname': 1, 
			'condition': 1, 
			'status': 1, 
			'date_json': 1, 
			'date_accept': 1, 
			'date_finish': 1
		  }
		}, {
		  '$lookup': {
			'from': 'skills', 
			'localField': 'skill_id', 
			'foreignField': '_id', 
			'as': 'skill'
		  }
		}, {
		  '$replaceRoot': {
			'newRoot': {
			  '$mergeObjects': [
				{
				  '$arrayElemAt': [
					'$skillname', 0
				  ]
				}, '$$ROOT'
			  ]
			}
		  }
		}, {
		  '$project': {
			'_id': 1, 
			'tutor_id': 1, 
			'student_id': 1, 
			'tutor_firstname': 1, 
			'tutor_lastname': 1, 
			'description': 1, 
			'address': 1, 
			'hourlyrate': 1, 
			'hours': 1, 
			'skill': 1, 
			'condition': 1, 
			'status': 1, 
			'date_json': 1, 
			'date_accept': 1, 
			'date_finish': 1
		  }
		}
	  ]).then((contract)=>{res.json(contract)})
})

// get specific contract of this student
router.get('/contract/:_id', (req,res,next)=>{
	Contract.aggregate([
		{
		  '$match': {
			'_id': req.params._id
		  }
		}, {
			'$lookup': {
			  'from': 'users', 
			  'localField': 'tutor_id', 
			  'foreignField': '_id', 
			  'as': 'tutor'
			}
		  }, {
			'$replaceRoot': {
			  'newRoot': {
				'$mergeObjects': [
				  {
					'$arrayElemAt': [
					  '$tutor', 0
					]
				  }, '$$ROOT'
				]
			  }
			}
		  }, {
			'$project': {
			  '_id': 1, 
			  'tutor_id': 1, 
			  'student_id': 1, 
			  'tutor_firstname': '$firstname', 
			  'tutor_lastname': '$lastname', 
			  'description': 1, 
			  'address': 1, 
			  'hourlyrate': 1, 
			  'hours': 1, 
			  'skill_id': 1, 
			  'skillname': 1, 
			  'condition': 1, 
			  'status': 1, 
			  'date_json': 1, 
			  'date_accept': 1, 
			  'date_finish': 1
			}
		  }, {
			'$lookup': {
			  'from': 'skills', 
			  'localField': 'skill_id', 
			  'foreignField': '_id', 
			  'as': 'skill'
			}
		  }, {
			'$replaceRoot': {
			  'newRoot': {
				'$mergeObjects': [
				  {
					'$arrayElemAt': [
					  '$skillname', 0
					]
				  }, '$$ROOT'
				]
			  }
			}
		  }, {
			'$project': {
			  '_id': 1, 
			  'tutor_id': 1, 
			  'student_id': 1, 
			  'tutor_firstname': 1, 
			  'tutor_lastname': 1, 
			  'description': 1, 
			  'address': 1, 
			  'hourlyrate': 1, 
			  'hours': 1, 
			  'skill': 1, 
			  'condition': 1, 
			  'status': 1, 
			  'date_json': 1, 
			  'date_accept': 1, 
			  'date_finish': 1
			}
		  }
	  ]).find({_id: req.body.id}, (err,contract)=>{
		if(err) return res.json(err)
		if(!contract) return res.json("No contract")
		else return res.json(contract)
 	})
})

router.get('/contract/post/tutor=:id',(req,res,next)=>{
	Contract.aggregate([
		{'$match': {'tutor_id': new mongoose.Types.ObjectId(req.params.id)}},
		{
		  '$project': {
			'tutor_id': 1, 
			'condition': 1, 
			'hourlyrate': 1, 
			'address': 1, 
			'skill_id': 1
		  }
		}, {
		  '$lookup': {
			'from': 'users', 
			'localField': 'tutor_id', 
			'foreignField': '_id', 
			'as': 'tutor'
		  }
		}, {
		  '$replaceRoot': {
			'newRoot': {
			  '$mergeObjects': [
				{
				  '$arrayElemAt': [
					'$tutor', 0
				  ]
				}, '$$ROOT'
			  ]
			}
		  }
		}, {
		  '$project': {
			'tutor_id': 1, 
			'condition': 1, 
			'hourlyrate': 1, 
			'address': 1, 
			'skill_id': 1, 
			'tutor_name': '$username'
		  }
		}, {
		  '$lookup': {
			'from': 'skills', 
			'localField': 'skill_id', 
			'foreignField': '_id', 
			'as': 'skills'
		  }
		}, {
		  '$project': {
			'tutor_id': 1, 
			'condition': 1, 
			'hourlyrate': 1, 
			'address': 1, 
			'skill_id': 1, 
			'tutor_name': 1, 
			'skills': 1
		  }
		}
	  ]).then(contract=>res.json(contract[0]))
})

router.post('/contract/post/tutor=:id', (req,res,next)=>{
	Skill.find({name: { $in:req.params.id}},(err,skill)=>{
		if(err) return res.json(err)
		if(!skill) return res.json("No skill was founded");
		else return skill
	}).then( async(skill)=>{
		if(!skill) return
		console.log(skill)
		skillid= skill.map(s=>s._id)
		let contract = new Contract({
			tutor_id: req.params.id,
			student_id: req.body.student_id,
			skill_id: sk4illid,
			hours: req.body.hours,
			hourlyrate: req.body.hourlyrate,
			condition: req.body.condition,
			address: req.body.address
		})
		try{
			const saved = await contract.save();
			res.json(saved);
		  }catch{
			 res.status(400).json(err);
		  }
		}
	)
})




//Facebook login
router.get('/account', ensureAuthenticated, function(req, res){
	res.json('account', { user: req.user });
  });

router.get('/auth/facebook', passport.authenticate('facebook',{scope:'email'}));

router.get('/auth/facebook/callback',
	passport.authenticate('facebook', { successRedirect : '/', failureRedirect: '/login' }),
	function(req, res) {
		res.redirect('/');
});

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) { return next(); }
	res.redirect('/login')
  }

// Google login

module.exports = router;
