var express = require('express');
var router = express.Router();
let passport = require("../middleware/passport")
let User= require('../models/user')
let User_detail = require('../models/user_detail')
let Skill = require('../models/skill')
let bcrypt = require('bcrypt')
let jwt= require('jsonwebtoken')

//get all tutor *all account and detail info* (CHECKED)
router.get('/', (req,res,next)=>{
    // User.find({type:2}, (err,user)=>{
    //     if(err) return res.json(err)
    //     if(!user) return res.json(user)
    //     else{
    //         let list=[]
    //         user.forEach(u => {
    //             User_detail.findOne({userId: u._id}).then(detail=>{
    //                 if(detail)
    //                     list.push({...u.toObject(), ...detail.toObject()})
    //                 else
    //                     list.push({...u.toObject()})
    //             })
    //         })
    //         res.json(list)
    //     }
    // })
   let offSet= parseInt(req.query.offSet)
   let indexStart= parseInt(req.query.indexStart)
   
    User.aggregate([
         {  $match: {"type": 2}  },
         {  $lookup: {
              from: "user_details",
              localField: "_id",    // field in the orders collection
              foreignField: "idUser",  // field in the items collection
              as: "detail"
           }
        },
        {
           $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$detail", 0 ] }, "$$ROOT" ] } }
        },
        { $project: { detail: 0 } }
     ]).then(tutor=>res.json({array:tutor.slice(indexStart,offSet+indexStart), pageCount:tutor.length})
   )})





module.exports = router;