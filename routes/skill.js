var express = require('express');
var router = express.Router();
let Skill = require('../models/skill');


// get skill list (CHECKED)
router.get('/skill', (req,res,next)=>{
    offSet= req.query.offSet
    indexStart=req.query.indexStart
    Skill.find((err,skill)=>{
      if(err) return res.json(err);
      if(!skill) return res.json('No speciality');
      else {
        let list = skill.slice(indexStart, offSet+indexStart+1)
        return res.json({array:list, pageCount:list.length});
      }
    })
  })

module.exports = router