let passport = require("passport");
let JwtStrategy = require("passport-jwt").Strategy;
let FacebookStrategy = require("passport-facebook").Strategy;
let ExtractJwt = require("passport-jwt").ExtractJwt;
let User = require('../models/user');
let dotenv= require('dotenv');
dotenv.config();

let config = process.env
let opts = {}

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.JWT_SECRET

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    User.findOne({email: jwt_payload.sub}, function(err, user) {
        if (err) {
            return done(err, false,  { message: 'You must login first' });
        }
        if (user) {
            require.user = user
            return done(null, user);
        } else {
            return done(null, false,  { message: 'You must login first' });
        }
    });
}));

passport.use(new FacebookStrategy({
    clientID: config.facebook_api_key,
    clientSecret:config.facebook_api_secret ,
    callbackURL: config.callback_url
  },
  function(accessToken, refreshToken, profile, done) {
    process.nextTick(function () {
      console.log(accessToken, refreshToken, profile, done);
      return done(null, profile);
    });
  }
));

passport.serializeUser(function(user, done) {
    done(null, user);
  });
  
passport.deserializeUser(function(obj, done) {
done(null, obj);
});


module.exports = passport;